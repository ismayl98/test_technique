<?php 
session_start();
if (! isset($_SESSION['user'])){
    $_SESSION['flash']['error'][] = 'Vous devez d\'abord vous connecter';
    header('Location: /login.php');
    exit();
}
?>
<!DOCTYPE html>
<html lang="fr">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="style.css">
    <title>Page privée</title>
</head>
<body>
    <div class="messages">
        <?php
            if (isset($_SESSION['flash'])){
                foreach ($_SESSION['flash'] as $type => $messages){
                    foreach($messages as $message){
                        echo("<div class=\"$type\">$message</div>");
                    }
                }
                unset($_SESSION['flash']);
            }
        ?>
    </div>
    <h2> Bienvenue sur votre page privée, <?=$_SESSION['user']['fullname']?> ! </h2>
    <a href="/logout.php">Se déconnecter</a>
</body>
</html>

Réponses aux questions:

Q1) trois exemple de design pattern: 
	Les design pattern sont classé par trois catégorie:
	--nous avons les pattern de type Création comme pa exemple Abstract;
	--ensuite les pattern de type Structure tel que Composite;
	--enfin les patter de type Comportement tel que Strategy.

Q2) les conditions fausses sont:
	(0123==123)===false car 0123==123 est vrai puisque l'opérateur '==' est un opérateur qui 		vérifie l'équivalence et non l'egalité stricte qui est vérifiée par l'opérateur '==='.
	 Par conséquent vrai===faux est une condition fausse.

Q3) Un cookie est un fichier dans lequel sont enregistrés des informations et qui se trouve sur l'ordinateur de l'utilisateur. En plus ce dernier a une durée de vie. Or la session est un moyen d'enregistrer les informations directement sur le web. c'est ce qui nous permet d'ailleurs de pouvoiir utiliser les informations des variables de sessions sur plusieurs pages.

Q4) 
 --Pour créer un table permettant cela, permettez moi de rappeler quelques notions sur le MCD et le MLD(model logique de données):
 	le problème posé est: on veut qu'un article soit associé à plusieurs catégories au lieu 		d'un seul. Mais aussi il faut savoir qu'une catégorie concerne plusieurs articles.
	Donc on a une relation de type R** : article * <--> * category.
	pour le passage au MLD, on aura donc la création d'une nouvelle table d'association, 	contenant les clé primaires des deux tables qui migrent dans la nouvelle table 		d'association en tant que clé etrangeres et qui forme une clé primaire de cette table.

on a donc 

	create table association(
		id_article varchar(255) references exercice_article(id),
		id_category varchar(255) references exercice_article_category(id),
		PRIMARY KEY (id_article,id_category) 
	);
	
 --Pour chaque category le nombre d'article qu'elle contient:
 
 SELECT category.title, count(id_article) FROM exercice_article_category AS category 
 	JOIN association AS assoc ON category.id = assoc.id_category 
	JOIN exercice_article AS article on article.id = assoc.id_article
	GROUP BY category.title;
	
 --L'article qui se trouve dans le plus de category
 
 SELECT article.title, MAX(COUNT(assoc.id_category))FROM exercice_article AS article 
 	JOIN association as assoc on article.id = assoc.id_article
	GROUP BY article.title


