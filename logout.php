<?php
session_start();

if (isset($_SESSION['user'])){
    unset($_SESSION['user']);
}

$_SESSION['flash']['success'][] = 'Vous êtes déconnecté';

header('Location: /login.php');
exit();