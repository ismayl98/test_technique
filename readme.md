Réponses aux questions:

Q1) trois exemple de design pattern: 
	Les design pattern sont classé par trois catégorie:
	--nous avons les pattern de type Création comme pa exemple Abstract;
	--ensuite les pattern de type Structure tel que Composite;
	--enfin les patter de type Comportement tel que Strategy.

Q2) les conditions fausses sont:
	(0123==123)===false car 0123==123 est vrai puisque l'opérateur '==' est un opérateur qui 		vérifie l'équivalence et non l'egalité stricte qui est vérifiée par l'opérateur '==='.
	 Par conséquent vrai===faux est une condition fausse.

Q3) Un cookie est un fichier dans lequel sont enregistrés des informations et qui se trouve sur l'ordinateur de l'utilisateur. En plus ce dernier a une durée de vie. Or la session est un moyen d'enregistrer les informations directement sur le web. c'est ce qui nous permet d'ailleurs de pouvoiir utiliser les informations des variables de sessions sur plusieurs pages.

Q4) 
 --Pour créer un table permettant cela, permettez moi de rappeler quelques notions sur le MCD et le MLD(model logique de données):
 	le problème posé est: on veut qu'un article soit associé à plusieurs catégories au lieu 		d'un seul. Mais aussi il faut savoir qu'une catégorie concerne plusieurs articles.
	Donc on a une relation de type R** : article * <--> * category.
	pour le passage au MLD, on aura donc la création d'une nouvelle table d'association, 	contenant les clé primaires des deux tables qui migrent dans la nouvelle table 		d'association en tant que clé etrangeres et qui forme une clé primaire de cette table.

on a donc 

	create table association(
		id_article varchar(255) references exercice_article(id),
		id_category varchar(255) references exercice_article_category(id),
		PRIMARY KEY (id_article,id_category) 
	);
	
 --Pour chaque category le nombre d'article qu'elle contient:
 
 SELECT category.title, count(id_article) FROM exercice_article_category AS category 
 	JOIN association AS assoc ON category.id = assoc.id_category 
	JOIN exercice_article AS article on article.id = assoc.id_article
	GROUP BY category.title;
	
 --L'article qui se trouve dans le plus de category
 
 SELECT article.title, MAX(COUNT(assoc.id_category))FROM exercice_article AS article 
 	JOIN association as assoc on article.id = assoc.id_article
	GROUP BY article.title

Pour le dernier exercice, allez dans un terminal:
	placez vous à la racine du projet et ensuite table la commande suivante:
	$ php -S localhost:8000
lancez un navigateur taper localhost:8000 dans la barre de l'url
j'ai crée une petite base de donnée SQLite3 nommée test.db et qui contient la table users qui se trouve à la racine du projet.
Si vous avez sqlite3 deja installé vous pouvez consulter le schema de la table directement sur le termial apres avoir lancé sqlite3. tapez la commande ".open test.db" pour ouvrir synchroniser avec la base de donnees deja existante. 
pour consulter le schema de la table taper la commande ".schema users"

