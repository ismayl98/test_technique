<?php
session_start();
if ($_SERVER['REQUEST_METHOD'] === 'POST') {
    if (isset($_POST['login']) && isset($_POST['password'])){
        
        $login = $_POST['login'];
        $password = $_POST['password'];

        $db = new SQLite3('test.db');
        $results = $db->query("SELECT * FROM users WHERE login='$login' and password='$password'"); // pas de hashage du mdp car il s'agit d'un test de connexion et pas d'inscription
        if ($user = $results->fetchArray()){
            $_SESSION['user'] = $user;
            header('Location: /private.php');
        } else {
            $_SESSION['flash']['error'][] = 'Nom d\'utilisateur ou mot de passe incorrecte';
        }
    }

}
?>
<!DOCTYPE html>
<html lang="fr">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="style.css">
    <title>Connexion</title>
</head>
<body>
    <center><h2> Page de connexion </h2></center>
    <div class="messages">
        <?php
            if (isset($_SESSION['flash'])){
                foreach ($_SESSION['flash'] as $type => $messages){
                    foreach($messages as $message){
                        echo("<div class=\"$type\">$message</div>");
                    }
                }
                unset($_SESSION['flash']);
            }
        ?>
    </div>
    <form id="loginform" action="/login.php" method="post">
        
        <div class="form-group">
            <label for="login">Nom d'utilisateur</label>
            <input type="text" name="login" id="login">
        </div>

        <div class="form-group">
            <label for="password">Mot de passe</label>
            <input type="password" name="password" id="password">
        </div>
        <div id="submitdiv" class="form-group">
            <input type="submit" value="Se connecter">
        </div>
    </form>
</body>
</html>
